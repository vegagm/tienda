#!/usr/bin/env python3


import sys

articulos = {}

def anadir(articulo, precio):
    articulos[articulo] = precio

def mostrar():
    print("La lista de articulos en la tienda: ")
    for articulo, precio in articulos.items():
        print(f"{articulo}: {precio:.2f}€")

def pedir_articulo() -> str:

    while True:
        articulo = input("Articulo: ")
        if articulo in articulos:
            return articulo

        else:
            print("El artículo no está en la tienda.")


def pedir_cantidad() -> float:

    while True:
        cantidad = input("Cantidad: ")

        try:
            cantidad = float(cantidad)
            return cantidad

        except ValueError:
            print("La cantidad no es un número real.")

def main():

    for i in range(1, len(sys.argv),2):
            articulo: str = sys.argv[i]
            precio = float(sys.argv[i+1])
            anadir(articulo, precio)

    print("Artículos disponibles:")
    mostrar()

    articulo = pedir_articulo()
    cantidad = pedir_cantidad()

    precio_unitario = articulos[articulo]
    precio_total = precio_unitario * cantidad

    print(f"Compra total: {cantidad} de {articulo} a {precio_unitario:.2f}€.")
    print(f"El precio total es: {precio_total:.2f}€")

if __name__ == '__main__':
     main()


